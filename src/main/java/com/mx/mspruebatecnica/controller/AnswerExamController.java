package com.mx.mspruebatecnica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.mspruebatecnica.dto.AnswerExamDTO;
import com.mx.mspruebatecnica.dto.EvaluateTestDTO;
import com.mx.mspruebatecnica.service.AnswerExamService;

@RestController
@CrossOrigin("*")
@RequestMapping("/ms-prueba-tecnica")
public class AnswerExamController {

	@Autowired
	AnswerExamService service;
	
	@PostMapping("/answerExam")
	public ResponseEntity<Object> answerExam( @RequestBody AnswerExamDTO dto) {
		try {
			EvaluateTestDTO result = service.answerExam(dto);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("" + e.getMessage(), HttpStatus.BAD_REQUEST);
		}

	}
}
