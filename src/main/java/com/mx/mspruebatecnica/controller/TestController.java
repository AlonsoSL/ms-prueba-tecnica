package com.mx.mspruebatecnica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.mspruebatecnica.dto.TestDTO;
import com.mx.mspruebatecnica.service.AssignTestService;
import com.mx.mspruebatecnica.service.TestService;

@RestController
@CrossOrigin("*")
@RequestMapping("/ms-prueba-tecnica")
public class TestController {

	@Autowired
	TestService service;
	
	@Autowired
	AssignTestService serviceScoreTest;
	
	@PostMapping("/newTest")
	public ResponseEntity<Object> newTest( @RequestBody TestDTO dto) {
		try {
			TestDTO result = service.newTest(dto);
			return new ResponseEntity<>(result, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("" + e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

}
