package com.mx.mspruebatecnica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.mspruebatecnica.service.StudentService;

@RestController
@CrossOrigin("*")
@RequestMapping("/ms-prueba-tecnica")
public class StatusController {

	@Autowired
	StudentService service;
	
	@GetMapping("/status")
	public String status() {
		System.out.println("LLEGO AL CONTROLLER");
		return "STATUS OK";
	}
	
	@GetMapping("/id")
	public Integer getid() {
		System.out.println("LLEGO AL CONTROLLER");
		return service.getid();
	}
}
