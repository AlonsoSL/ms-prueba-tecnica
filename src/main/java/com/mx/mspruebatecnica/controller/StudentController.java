package com.mx.mspruebatecnica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.mspruebatecnica.dto.StudentDTO;
import com.mx.mspruebatecnica.service.StudentService;

@RestController
@CrossOrigin("*")
@RequestMapping("/ms-prueba-tecnica")
public class StudentController {
	@Autowired
	StudentService service;
	
	@PostMapping("/newStudent")
	public ResponseEntity<Object> newStudent( @RequestBody StudentDTO dto) {
		try {
			StudentDTO studenCreated = service.newStudent(dto);
			return new ResponseEntity<>(studenCreated, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("" + e.getMessage(), HttpStatus.BAD_REQUEST);
		}

	}

}
