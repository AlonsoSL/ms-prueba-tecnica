package com.mx.mspruebatecnica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.mspruebatecnica.dto.AssignTestDTO;
import com.mx.mspruebatecnica.service.AssignTestService;

@RestController
@CrossOrigin("*")
@RequestMapping("/ms-prueba-tecnica")
public class AssignTestController {

	@Autowired
	AssignTestService service;
	
	@PostMapping("/assignTest")
	public ResponseEntity<Object> assignTest( @RequestBody AssignTestDTO dto) {
		try {
			AssignTestDTO result = service.assignTest(dto);
			return new ResponseEntity<>(result, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("" + e.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}
}
