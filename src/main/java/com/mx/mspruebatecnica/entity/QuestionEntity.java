package com.mx.mspruebatecnica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PREGUNTA_EXAMEN")
public class QuestionEntity {

	@Id
	@Column(name = "PREGUNTA_ID")
	private Integer questionId;

	@Column(name = "EXAMEN_ID")
	private Integer testId;

	@Column(name = "PUNTUAJE")
	private Integer score;

	@Column(name = "PREGUNTA")
	private String question;

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public Integer getTestId() {
		return testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public QuestionEntity(Integer questionId, Integer testId, Integer score, String question) {
		super();
		this.questionId = questionId;
		this.testId = testId;
		this.score = score;
		this.question = question;
	}

	public QuestionEntity() {
		super();
	}

}
