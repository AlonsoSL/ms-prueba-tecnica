package com.mx.mspruebatecnica.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface QuestionRepositoryDAO extends JpaRepository<QuestionEntity, String>{

	@Query(value = "SELECT SEQUENCE_PREGUNTA_EXAMEN.nextval from dual", nativeQuery =true)
	public Integer getNextValLog();
}
