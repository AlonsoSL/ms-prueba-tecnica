package com.mx.mspruebatecnica.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TestEstudentRepositoryDAO  extends JpaRepository<TestEstudentEntity, String>{

	@Query(value = "SELECT SEQUENCE_EXAMEN_ESTUDIANTE.nextval from dual", nativeQuery =true)
	public Integer getNextValLog();
	
	@Query(value="SELECT NEW com.mx.mspruebatecnica.entity.TestEstudentEntity(u.id, u.testId, u.studentId, u.date, u.testScore ) FROM TestEstudentEntity u WHERE u.testId =:testId AND u.studentId =:studentId ")
	TestEstudentEntity getTest(Integer testId, Integer studentId );
}
