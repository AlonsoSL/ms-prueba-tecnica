package com.mx.mspruebatecnica.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface ResponseRepositoryDAO extends JpaRepository<ResponseEntity, String>{

	@Query(value = "SELECT SEQUENCE_RESPUESTA_EXAMEN.nextval from dual", nativeQuery =true)
	public Integer getNextValLog();
	 
	@Query(value="SELECT NEW com.mx.mspruebatecnica.entity.ResponseEntity(u.responseId, u.questionId, u.testId, u.response, u.status, u.score ) FROM ResponseEntity u WHERE u.responseId =:id ")
	ResponseEntity getResponseById(Integer id);
}
