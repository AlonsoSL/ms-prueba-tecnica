package com.mx.mspruebatecnica.entity;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface StudentRepositoryDAO extends JpaRepository<StudentEntity, String>{


	@Query(value = "SELECT SEQUENCE_ESTUDIANTE.nextval from dual", nativeQuery =true)
	public Integer getNextValLog();
	
	@Query(value="SELECT NEW com.mx.mspruebatecnica.entity.StudentEntity(u.studentId, u.name, u.age, u.city, u.timeZone ) FROM StudentEntity u WHERE u.studentId = :studentId ")
	StudentEntity findStudent(Integer studentId);
}
