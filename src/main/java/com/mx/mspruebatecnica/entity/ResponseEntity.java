package com.mx.mspruebatecnica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "RESPUESTA_EXAMEN")
public class ResponseEntity {

	@Id
	@Column(name = "RESPUESTA_ID")
	private Integer responseId;

	@Column(name = "PREGUNTA_ID")
	private Integer questionId;

	@Column(name = "EXAMEN_ID")
	private Integer testId;

	@Column(name = "RESPUESTA")
	private String response;

	@Column(name = "ESTADO")
	private String status;
	
	@Column(name = "PUNTUAJE")
	private Integer score;

	public Integer getResponseId() {
		return responseId;
	}

	public void setResponseId(Integer responseId) {
		this.responseId = responseId;
	}

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public Integer getTestId() {
		return testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public ResponseEntity(Integer responseId, Integer questionId, Integer testId, String response, String status, Integer score) {
		super();
		this.responseId = responseId;
		this.questionId = questionId;
		this.testId = testId;
		this.response = response;
		this.status = status;
		this.score = score;
	}

	public ResponseEntity() {
		super();
	}

}
