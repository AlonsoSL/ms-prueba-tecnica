package com.mx.mspruebatecnica.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TestRepositoryDAO extends JpaRepository<TestEntity, String>{

	@Query(value = "SELECT SEQUENCE_EXAMEN.nextval from dual", nativeQuery =true)
	public Integer getNextValLog();
	
	@Query(value="SELECT NEW com.mx.mspruebatecnica.entity.TestEntity(u.testId, u.testName, u.testDate ) FROM TestEntity u WHERE u.testId =:testId ")
	TestEntity findTestById(Integer testId);
}
