package com.mx.mspruebatecnica.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ESTUDIANTE")
public class StudentEntity implements Serializable{

    private static final long serialVersionUID = 8276249731638378452L;

	@Id
	@Column(name = "ESTUDIANTE_ID")
	private Integer studentId;

	@Column(name = "NOMBRE")
	private String name;

	@Column(name = "EDAD")
	private Integer age;

	@Column(name = "CIUDAD")
	private String city;

	@Column(name = "ZONA_HORARIA")
	private String timeZone;

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public StudentEntity(Integer studentId, String name, Integer age, String city, String timeZone) {
		super();
		this.studentId = studentId;
		this.name = name;
		this.age = age;
		this.city = city;
		this.timeZone = timeZone;
	}

	public StudentEntity() {
		super();
	}

}
