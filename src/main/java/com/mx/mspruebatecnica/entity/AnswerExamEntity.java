package com.mx.mspruebatecnica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "RESPUESTA_ESTUDIANTE")
public class AnswerExamEntity {

	@Id
	@Column(name = "ID")
	private Integer id;

	@Column(name = "EXAMEN_ID")
	private Integer testId;
	@Column(name = "NOMBRE_EXAMEN")
	private String testName;

	@Column(name = "PREGUNTA_ID")
	private Integer questionId;
	@Column(name = "PREGUNTA")
	private String question;

	@Column(name = "RESPUESTA_ID")
	private Integer responseId;
	@Column(name = "RESPUESTA")
	private String response;

	@Column(name = "PUNTUAJE")
	private Integer score;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTestId() {
		return testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public Integer getResponseId() {
		return responseId;
	}

	public void setResponseId(Integer responseId) {
		this.responseId = responseId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public AnswerExamEntity(Integer id, Integer testId, String testName, Integer questionId, String question,
			Integer responseId, String response, Integer score) {
		super();
		this.id = id;
		this.testId = testId;
		this.testName = testName;
		this.questionId = questionId;
		this.question = question;
		this.responseId = responseId;
		this.response = response;
		this.score = score;
	}

	public AnswerExamEntity() {
		super();
	}

}
