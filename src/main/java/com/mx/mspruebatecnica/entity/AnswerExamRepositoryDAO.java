package com.mx.mspruebatecnica.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AnswerExamRepositoryDAO extends JpaRepository<AnswerExamEntity, String> {

	@Query(value = "SELECT SEQUENCE_RESPUESTA_ESTUDIANTE.nextval from dual", nativeQuery =true)
	public Integer getNextValLog();
}
