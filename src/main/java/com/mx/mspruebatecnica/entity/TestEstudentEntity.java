package com.mx.mspruebatecnica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EXAMEN_ESTUDIANTE")
public class TestEstudentEntity {

	@Id
	@Column(name = "ID")
	private Integer id;

	@Column(name = "EXAMEN_ID")
	private Integer testId;

	@Column(name = "ESTUDIANTE_ID")
	private Integer studentId;

	@Column(name = "FECHA")
	private String date;
	
	@Column(name = "CALIFICACION")
	private Integer testScore;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTestId() {
		return testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getTestScore() {
		return testScore;
	}

	public void setTestScore(Integer testScore) {
		this.testScore = testScore;
	}

	public TestEstudentEntity(Integer id, Integer testId, Integer studentId, String date, Integer testScore) {
		super();
		this.id = id;
		this.testId = testId;
		this.studentId = studentId;
		this.date = date;
		this.testScore = testScore;
	}

	public TestEstudentEntity() {
		super();
	}

}
