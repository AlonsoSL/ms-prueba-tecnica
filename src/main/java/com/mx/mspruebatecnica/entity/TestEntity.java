package com.mx.mspruebatecnica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "EXAMEN")
public class TestEntity {

	@Id
	@Column(name = "EXAMEN_ID")
	private Integer testId;

	@Column(name = "NOMBRE_EXAMEN")
	private String testName;
	
	@Column(name = "FECHA_EXAMEN")
	private String testDate;

	public Integer getTestId() {
		return testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public String getTestDate() {
		return testDate;
	}

	public void setTestDate(String testDate) {
		this.testDate = testDate;
	}

	public TestEntity(Integer testId, String testName, String testDate) {
		super();
		this.testId = testId;
		this.testName = testName;
		this.testDate = testDate;
	}

	public TestEntity() {
		super();
	}

}
