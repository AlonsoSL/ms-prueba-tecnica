package com.mx.mspruebatecnica.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.mspruebatecnica.dto.QuestionDTO;
import com.mx.mspruebatecnica.entity.QuestionEntity;
import com.mx.mspruebatecnica.entity.QuestionRepositoryDAO;
import com.mx.mspruebatecnica.exception.BadRequestException;

@Service
public class QuestionServiceImpl implements QuestionService {

	@Autowired
	QuestionRepositoryDAO  questionRepositoryDAO;
	@Autowired
	ResponseService responseService;
	
	@Override
	public void newQuestions(List<QuestionDTO> dto, Integer testId) {
		
		validateResponsesNumber(dto);
		for(QuestionDTO req: dto) {
			Integer questionId = questionRepositoryDAO.getNextValLog();
			
			responseService.newResponses(req.getResponses(), testId, questionId, req.getScore());
			
			QuestionEntity entity = new QuestionEntity();
			entity.setQuestionId(questionId);
			entity.setTestId(testId);
			entity.setScore(req.getScore());
			entity.setQuestion(req.getQuestion());

			questionRepositoryDAO.save(entity);
		}
	}
	
	
	 private void validateResponsesNumber(List<QuestionDTO> questionList){
		Integer total= 0;
		for(QuestionDTO question : questionList) {
			total = question.getResponses().size();
			if(total>4 || total<4) {
				throw new BadRequestException("deben existir cuatro posibles respuestas por pregunta");
			}
		}

	}

}
