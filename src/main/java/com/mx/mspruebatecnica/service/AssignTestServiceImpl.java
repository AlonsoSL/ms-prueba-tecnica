package com.mx.mspruebatecnica.service;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.mspruebatecnica.dto.AssignTestDTO;
import com.mx.mspruebatecnica.dto.EvaluateTestDTO;
import com.mx.mspruebatecnica.entity.StudentEntity;
import com.mx.mspruebatecnica.entity.StudentRepositoryDAO;
import com.mx.mspruebatecnica.entity.TestEntity;
import com.mx.mspruebatecnica.entity.TestEstudentEntity;
import com.mx.mspruebatecnica.entity.TestEstudentRepositoryDAO;
import com.mx.mspruebatecnica.entity.TestRepositoryDAO;
import com.mx.mspruebatecnica.exception.NotFoundException;

@Service
public class AssignTestServiceImpl  implements AssignTestService{

	@Autowired
	TestEstudentRepositoryDAO testEstudentRepositoryDAO;
	
	@Autowired
	TestRepositoryDAO testRespositoryDAO;
	
	@Autowired
	StudentRepositoryDAO studentRepositoryDAO;
	
	@Autowired
	TimeZoneService timeZoneService;
	
	@Override
	public AssignTestDTO assignTest(AssignTestDTO dto) {
		Integer nextId = testEstudentRepositoryDAO.getNextValLog();
		String dateTest ="";
		TestEntity result =testRespositoryDAO.findTestById(dto.getTestId());
		StudentEntity student = studentRepositoryDAO.findStudent(dto.getStudentId());
		
		try {
			dateTest= timeZoneService.changeTimeZone(result.getTestDate(), student.getTimeZone());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TestEstudentEntity entity = new TestEstudentEntity();
		entity.setId(nextId);
		entity.setTestId(dto.getTestId());
		entity.setStudentId(dto.getStudentId());
		entity.setDate(dateTest);

		testEstudentRepositoryDAO.save(entity);
		return dto;
	}

	@Override
	public void evaluateTest(EvaluateTestDTO dto) {
		
		TestEstudentEntity entity = testEstudentRepositoryDAO.getTest(dto.getTestId(), dto.getStudentId());
		

		if (entity == null) {
			throw new NotFoundException("no se encontro el examen para el estudiante");
		}
		entity.setTestScore(dto.getTestScore());
		testEstudentRepositoryDAO.save(entity);
	}

}
