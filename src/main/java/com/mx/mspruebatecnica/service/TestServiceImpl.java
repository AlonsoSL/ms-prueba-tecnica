package com.mx.mspruebatecnica.service;

import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.mspruebatecnica.dto.QuestionDTO;
import com.mx.mspruebatecnica.dto.TestDTO;
import com.mx.mspruebatecnica.entity.TestEntity;
import com.mx.mspruebatecnica.entity.TestEstudentRepositoryDAO;
import com.mx.mspruebatecnica.entity.TestRepositoryDAO;
import com.mx.mspruebatecnica.exception.BadRequestException;

@Service
public class TestServiceImpl implements TestService {

	@Autowired
	TestRepositoryDAO testRespositoryDAO;

	@Autowired
	QuestionService questionService;
	
	@Autowired
	TestEstudentRepositoryDAO testEstudentRepositoryDAO;

	@Override
	public TestDTO newTest(TestDTO dto) {
		Integer nextTestId = testRespositoryDAO.getNextValLog();

		validateScore(dto.getQuestions());
		questionService.newQuestions(dto.getQuestions(), nextTestId);

		TestEntity entity = new TestEntity();
		entity.setTestId(nextTestId);
		entity.setTestName(dto.getTestName());
		entity.setTestDate(dto.getTestDate());

		testRespositoryDAO.save(entity);
		return dto;
	}
	

	private void validateScore(List<QuestionDTO> questionList) {
		Integer total = 0;
		for (QuestionDTO question : questionList) {
			total = total + question.getScore();
		}
		if (total > 100 || total < 100) {
			throw new BadRequestException("la suma total de la puntuacion por pregunta debe ser igual a 100%");
		}
	}

	private void validateTestDate(String date) {
	    try {
	    	SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd hh:mm");
	        formatoFecha.setLenient(false);
	        formatoFecha.parse(date);

	    } catch (Exception e) {
			throw new BadRequestException("la fecha debe seguir el sig formato yyyy-MM-dd hh:mm");

	    }
	}

	
}
