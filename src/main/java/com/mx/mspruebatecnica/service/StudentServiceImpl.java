package com.mx.mspruebatecnica.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.mspruebatecnica.entity.StudentEntity;
import com.mx.mspruebatecnica.entity.StudentRepositoryDAO;
import com.mx.mspruebatecnica.dto.StudentDTO;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	StudentRepositoryDAO studentRepositoryDAO;

	@Override
	public StudentDTO newStudent(StudentDTO dto) {
		
		Integer nextSequenceValLog = studentRepositoryDAO.getNextValLog();

		StudentEntity entity = new StudentEntity();
		entity.setStudentId(nextSequenceValLog);
		entity.setName(dto.getName());
		entity.setAge(dto.getAge());
		entity.setCity(dto.getCity());
		entity.setTimeZone(dto.getTimeZone());

		studentRepositoryDAO.save(entity);
		return dto;
	}

	@Override
	public Integer getid() {
		Integer nextSequenceValLog = studentRepositoryDAO.getNextValLog();
		System.out.println("***************  "+nextSequenceValLog);
       return nextSequenceValLog;
	}
}
