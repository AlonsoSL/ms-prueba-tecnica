package com.mx.mspruebatecnica.service;

import com.mx.mspruebatecnica.dto.StudentDTO;

public interface StudentService {

	public StudentDTO newStudent(StudentDTO dto);
	public Integer getid( );
}
