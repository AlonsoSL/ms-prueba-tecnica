package com.mx.mspruebatecnica.service;

import java.util.List;

import com.mx.mspruebatecnica.dto.ResponseDTO;

public interface ResponseService {

	public void newResponses(List<ResponseDTO> dto, Integer testId, Integer questionId, Integer score);
}
