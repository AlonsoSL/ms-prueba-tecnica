package com.mx.mspruebatecnica.service;

import com.mx.mspruebatecnica.dto.AnswerExamDTO;
import com.mx.mspruebatecnica.dto.EvaluateTestDTO;

public interface AnswerExamService {

	public EvaluateTestDTO answerExam(AnswerExamDTO dto);

}
