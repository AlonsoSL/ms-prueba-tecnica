package com.mx.mspruebatecnica.service;

import com.mx.mspruebatecnica.dto.AssignTestDTO;
import com.mx.mspruebatecnica.dto.EvaluateTestDTO;

public interface AssignTestService {
	
	public AssignTestDTO assignTest(AssignTestDTO dto);
	
	public void evaluateTest(EvaluateTestDTO dto);

}
