package com.mx.mspruebatecnica.service;

import java.util.List;

import com.mx.mspruebatecnica.dto.QuestionDTO;

public interface QuestionService {

	public void newQuestions(List<QuestionDTO> dto, Integer testId);
}
