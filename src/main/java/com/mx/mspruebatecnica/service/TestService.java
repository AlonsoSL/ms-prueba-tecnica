package com.mx.mspruebatecnica.service;

import com.mx.mspruebatecnica.dto.TestDTO;

public interface TestService {
	
	public TestDTO newTest(TestDTO dto);
}
