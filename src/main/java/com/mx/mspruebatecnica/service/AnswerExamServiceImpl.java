package com.mx.mspruebatecnica.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.mspruebatecnica.dto.AnswerExamDTO;
import com.mx.mspruebatecnica.dto.AnswersDTO;
import com.mx.mspruebatecnica.dto.EvaluateTestDTO;
import com.mx.mspruebatecnica.entity.AnswerExamEntity;
import com.mx.mspruebatecnica.entity.AnswerExamRepositoryDAO;
import com.mx.mspruebatecnica.entity.ResponseEntity;
import com.mx.mspruebatecnica.entity.ResponseRepositoryDAO;

@Service
public class AnswerExamServiceImpl implements AnswerExamService {

	@Autowired
	AnswerExamRepositoryDAO answerExamRepositoryDAO;
	
	@Autowired
	ResponseRepositoryDAO responseRepositoryDAO;
	
	@Autowired
	AssignTestService service;
	
	@Override
	public EvaluateTestDTO answerExam(AnswerExamDTO dto) {
		
		Integer examScore=0;
		for(AnswersDTO answer: dto.getAnswers()) {
			Integer nextId = answerExamRepositoryDAO.getNextValLog();
			
			ResponseEntity response = responseRepositoryDAO.getResponseById(answer.getResponseId());
			
			AnswerExamEntity entity = new AnswerExamEntity();
			entity.setId(nextId);
			entity.setTestId(dto.getTestId());
			entity.setTestName(dto.getTestName());
			entity.setQuestionId(answer.getQuestionId());
			entity.setQuestion(answer.getQuestion());
			entity.setResponseId(answer.getResponseId());
			entity.setResponse(answer.getResponse());
			entity.setScore(response.getScore());

			examScore = examScore +response.getScore();
			answerExamRepositoryDAO.save(entity);
			
		}

		EvaluateTestDTO evaluate= new EvaluateTestDTO();
		evaluate.setTestId(dto.getTestId());
		evaluate.setTestName(dto.getTestName());
		evaluate.setStudentId(dto.getStudentId());
		evaluate.setStudentName(dto.getStudentName());
		evaluate.setTestScore(examScore);
		
		service.evaluateTest(evaluate);
		
		return evaluate;
	}

}
