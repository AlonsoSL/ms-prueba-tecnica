package com.mx.mspruebatecnica.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mx.mspruebatecnica.dto.ResponseDTO;
import com.mx.mspruebatecnica.entity.ResponseEntity;
import com.mx.mspruebatecnica.entity.ResponseRepositoryDAO;

@Service
public class ResponseServiceImpl implements ResponseService{

	@Autowired
	ResponseRepositoryDAO responseRepositoryDAO;
	
	@Override
	public void newResponses(List<ResponseDTO> dto, Integer testId, Integer questionId, Integer score) {		
		for(ResponseDTO res: dto) {
			Integer nextSequenceValLog = responseRepositoryDAO.getNextValLog();
			
			ResponseEntity entity = new ResponseEntity();
			entity.setResponseId(nextSequenceValLog);
			entity.setQuestionId(questionId);
			entity.setTestId(testId);
			entity.setResponse(res.getResponse());
			entity.setStatus(res.getStatus());
			entity.setScore(res.getStatus().equals("TRUE")?score:0);

			responseRepositoryDAO.save(entity);
			
		}


		
	}

}
