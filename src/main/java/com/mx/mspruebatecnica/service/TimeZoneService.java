package com.mx.mspruebatecnica.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.stereotype.Service;

@Service
public class TimeZoneService {

	 private static final String DATE_FORMAT = "dd-MM-yyyy hh:mm:ss a";

	  public String changeTimeZone(String dateTest, String tz) throws ParseException {	        
	        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");
    		Date date1 = format.parse(dateTest);
    		
	        // format with tz
	        TimeZone timeZone = TimeZone.getTimeZone(tz);
	        SimpleDateFormat formatterWithTimeZone = new SimpleDateFormat(DATE_FORMAT);
	        formatterWithTimeZone.setTimeZone(timeZone);

	        // change tz using formatter
	        String sDate = formatterWithTimeZone.format(date1);

	        // string to object date
	        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
	        Date dateWithTimeZone = formatter.parse(sDate); // string to Date Object

	        System.out.println("The actual date is: " + formatter.format(date1));
	        System.out.println("The date in " + timeZone.getID() + " is: " + sDate);
	        System.out.println("Object date: " + formatter.format(dateWithTimeZone));
	        return sDate;

	    }
}
