package com.mx.mspruebatecnica.dto;

public class StudentDTO {

	private String name;
	private Integer age;
	private String city;
	private String timeZone;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public StudentDTO() {
		super();
	}

	public StudentDTO(String name, Integer age, String city, String timeZone) {
		super();
		this.name = name;
		this.age = age;
		this.city = city;
		this.timeZone = timeZone;
	}

}
