package com.mx.mspruebatecnica.dto;

public class AnswersDTO {

	private Integer questionId;
	private String question;
	private Integer responseId;
	private String response;
	
	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public Integer getResponseId() {
		return responseId;
	}

	public void setResponseId(Integer responseId) {
		this.responseId = responseId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public AnswersDTO(Integer questionId, String question, Integer responseId, String response) {
		super();
		this.questionId = questionId;
		this.question = question;
		this.responseId = responseId;
		this.response = response;
	}

	public AnswersDTO() {
		super();
	}
	

}
