package com.mx.mspruebatecnica.dto;

import java.util.List;

public class AnswerExamDTO {

	private Integer testId;
	private String testName;
	private Integer studentId;
	private String studentName;
	private List<AnswersDTO> answers;

	public Integer getTestId() {
		return testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public List<AnswersDTO> getAnswers() {
		return answers;
	}

	public void setAnswers(List<AnswersDTO> answers) {
		this.answers = answers;
	}

	public AnswerExamDTO(Integer testId, String testName, Integer studentId, String studentName,
			List<AnswersDTO> answers) {
		super();
		this.testId = testId;
		this.testName = testName;
		this.studentId = studentId;
		this.studentName = studentName;
		this.answers = answers;
	}

	public AnswerExamDTO() {
		super();
	}

}
