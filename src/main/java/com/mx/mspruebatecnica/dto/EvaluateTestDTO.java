package com.mx.mspruebatecnica.dto;

public class EvaluateTestDTO {

	private Integer testId;
	private String testName;
	private Integer studentId;
	private String studentName;
	private Integer testScore;

	public Integer getTestId() {
		return testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public Integer getTestScore() {
		return testScore;
	}

	public void setTestScore(Integer testScore) {
		this.testScore = testScore;
	}

	public EvaluateTestDTO(Integer testId, String testName, Integer studentId, String studentName, Integer testScore) {
		super();
		this.testId = testId;
		this.testName = testName;
		this.studentId = studentId;
		this.studentName = studentName;
		this.testScore = testScore;
	}

	public EvaluateTestDTO() {
		super();
	}
}
