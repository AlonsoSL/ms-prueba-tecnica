package com.mx.mspruebatecnica.dto;

public class ResponseDTO {

	private String response;
	private String status;


	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ResponseDTO( String response, String status) {
		super();
		this.response = response;
		this.status = status;
	}

	public ResponseDTO() {
		super();
	}

}
