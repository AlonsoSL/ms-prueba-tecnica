package com.mx.mspruebatecnica.dto;

import java.util.List;

public class QuestionDTO {

	private Integer score;
	private String question;
	private List<ResponseDTO> responses;

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}



	public List<ResponseDTO> getResponses() {
		return responses;
	}

	public void setResponses(List<ResponseDTO> responses) {
		this.responses = responses;
	}

	public QuestionDTO(Integer score, String question, List<ResponseDTO> responses) {
		super();
		this.score = score;
		this.question = question;
		this.responses = responses;
	}

	public QuestionDTO() {
		super();
	}
}
