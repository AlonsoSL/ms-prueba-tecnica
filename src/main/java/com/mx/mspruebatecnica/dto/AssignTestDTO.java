package com.mx.mspruebatecnica.dto;

public class AssignTestDTO {

	private Integer testId;
	private Integer studentId;

	public Integer getTestId() {
		return testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public AssignTestDTO(Integer testId, Integer studentId ) {
		super();
		this.testId = testId;
		this.studentId = studentId;
	}

	public AssignTestDTO() {
		super();
	}
}
