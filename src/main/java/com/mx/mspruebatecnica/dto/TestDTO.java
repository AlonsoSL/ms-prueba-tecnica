package com.mx.mspruebatecnica.dto;

import java.util.List;

public class TestDTO {

	private String testName;
	private String testDate;
	private List<QuestionDTO> questions;

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public List<QuestionDTO> getQuestions() {
		return questions;
	}

	public void setQuestions(List<QuestionDTO> questions) {
		this.questions = questions;
	}

	public String getTestDate() {
		return testDate;
	}

	public void setTestDate(String testDate) {
		this.testDate = testDate;
	}

	public TestDTO(String testName, String testDate, List<QuestionDTO> questions) {
		super();
		this.testName = testName;
		this.testDate = testDate;
		this.questions = questions;
	}


	public TestDTO() {
		super();
	}

}
